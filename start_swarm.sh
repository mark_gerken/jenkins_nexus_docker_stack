#!/bin/bash

docker swarm init
docker-compose build
docker network rm devops_pipeline
docker network create --driver overlay --attachable devops_pipeline

docker stack rm devops_stack
docker stack deploy devops_stack --compose-file=./docker-swarm-compose.yml
