# DevOps Pipeline Sandbox

This repo is meant to create an out-of-the-box functioning set of services which can be leveraged as part of a devops pipeline. They're networked together using a Docker network named "devops_pipeline". The log in information for each service is "admin/admin". There is a single build defined in the jenkins service which points to the repo here: https://gitlab.com/mark_gerken/enigma and leverages the jenkins file in that repo.

## docker compose 2.x

'start_compose.sh' should create the devops_pipeline network and spin up containers for each service using docker-compose.

## docker swarm and compose 3.x

'start_swarm.sh' should create the devops_pipeline network and spin up containers for each service using docker swarm.