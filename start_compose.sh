docker-compose down
docker-compose build
docker network rm devops_pipeline
docker network create --driver bridge --attachable devops_pipeline
docker-compose up